## Installation

#based on git@bitbucket.org:paulroon/metrics.git (0.2 as a base)

1) Add library to your composer.json

{
    "require-dev": {
        "timhc22/debugger": "0.1.*",
    },
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:timhc22/debugger.git"
        }
    ]
}

2) run `composer update`

3) Add additional parameters needed to your services.yml

### not up to date ###

services:
    debugger:
        class: %debugger.class%
        arguments: [@request]
        calls:
            - [ enabled,[ %enable_debugger% ] ]
        public: true
        scope: request

    debugger.listener:
        class: %debugger.listener.class%
        tags:
            - { name: kernel.event_listener, event: kernel.request, method: onKernelRequest }
            - { name: kernel.event_listener, event: kernel.response, method: onKernelResponse }
        calls:
            - [ setContainer,[ @service_container ] ]


4) Configuration in app/config/config_dev.yml


5) Clear cache

